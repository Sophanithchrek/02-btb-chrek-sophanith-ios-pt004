//
//  ViewController.swift
//  PracticeFBLogin
//
//  Created by SOPHANITH CHREK on 26/11/20.
//

import UIKit
// Swift
//
// Add this to the header of your file, e.g. in ViewController.swift

import FBSDKLoginKit

// Add this to the body
class ViewController: UIViewController, LoginButtonDelegate {

    @IBOutlet weak var btnLoginFB: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    
    let loginButton = FBLoginButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if let token = AccessToken.current, !token.isExpired {
            // User is logged in, do work such as go to next view controller.
            let token = token.tokenString
            let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "id, email, name, picture"], tokenString: token, version: nil, httpMethod: .get)
            request.start(completionHandler: {connection, result, error in
                print("\(result!)")
            })
        } else {
            loginButton.center = view.center
            loginButton.delegate = self
            loginButton.isHidden = true
            loginButton.permissions = ["public_profile","email"]
            view.addSubview(loginButton)
        }
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        let token = result?.token?.tokenString
        
        let request = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                         parameters: ["fields": "id, email, name, picture.type(large)"],
                                                         tokenString: token,
                                                         version: nil,
                                                         httpMethod: .get)
        request.start(completionHandler: {connection, result, error in
            
            let object = result as? [String: Any]
            self.lblName.text = (object!["name"] as! String)
            self.lblEmail.text = (object!["email"] as! String)
            
            let picture = object!["picture"] as? [String: Any]
            let data = picture!["data"] as? [String: Any]
            let url = data!["url"]!
                        
            let myUrl = URL(string: url as! String)
                        
            let dataR = try? Data(contentsOf: myUrl!)
            let image = UIImage(data: dataR!)
            self.userProfileImageView.image = image
            
            self.btnLoginFB.setTitle("Logout", for: .normal)

        })
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        self.btnLoginFB.setTitle("Login With FB", for: .normal)
        self.lblName.text = "Default Name ..."
        self.lblEmail.text = "Default Email ..."
        self.userProfileImageView.image = UIImage(systemName: "person.fill")
    }
    
    @IBAction func btnLoginFBPressed(_ sender: Any) {
        loginButton.sendActions(for: .touchUpInside)
    }
}


